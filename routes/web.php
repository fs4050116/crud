<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('browse-products', 'ProductController@show')->name('browse-products');
// Route::get('edit-products/{id}', 'ProductController@edit')->name('edit');
// Route::post('update-products/{id}', 'ProductController@update')->name('update-product');







Route::group(['middleware' => ['auth']], function () {
// Route::get('add-product', 'ProductController@create')->name('add-product');
// Route::post('store-product', 'ProductController@store')->name('store-product');

Route::resource('product' , 'ProductController');
});
